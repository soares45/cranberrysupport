package ca.cranberry.app.beans;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Commentaire {

    @Id
    @GeneratedValue(strategy=GenerationType.TABLE)
    private int id;
    private String note;
    @OneToOne(fetch = FetchType.LAZY)
    private Utilisateur auteur;
    @ManyToOne
    @JoinColumn(name = "requete_id")
    private Requete requete;
    
    public Commentaire() {
        super();
    }

    Commentaire(String note, Utilisateur auteur) {
        this.note = note;
        this.auteur = auteur;
    }

    public Utilisateur getAuteur() {
        return auteur;
    }

    public String getNote() {
        return note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}