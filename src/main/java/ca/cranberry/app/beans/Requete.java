package ca.cranberry.app.beans;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Requete {

    public enum Statut {

        ouvert("ouvert"), enTraitement("en traitement"), finalAbandon("Abandon"), finalSucces("Succès");
        String valeur;

        Statut(String s) {
            this.valeur = s;
        }

        public String getValueString() {
            return valeur;
        }

        public static Statut fromString(String text) {
            if (text != null) {
                if (Statut.ouvert.getValueString().equals(text)) {
                    return Statut.ouvert;
                } else if (Statut.enTraitement.getValueString().equals(text)) {
                    return Statut.enTraitement;
                } else if (Statut.finalAbandon.getValueString().equals(text)) {
                    return Statut.finalAbandon;
                } else if (Statut.finalSucces.getValueString().equals(text)) {
                    return Statut.finalSucces;
                } else if (Statut.finalAbandon.getValueString().equals(text)) {
                    return Statut.finalAbandon;
                }

            }
            return null;
        }
    }

    public enum Categorie {

        posteDeTravail("Poste de travail"), serveur("Serveur"), serviceWeb("Service web"), compteUsager(
                "Compte usager"), autre("Autre");
        String value;

        Categorie(String s) {
            this.value = s;
        }

        public String getValueString() {
            return value;
        }

        public static Categorie fromString(String text) {
            if (text != null) {
                if (Categorie.posteDeTravail.getValueString().equals(text)) {
                    return Categorie.posteDeTravail;
                } else if (Categorie.serveur.getValueString().equals(text)) {
                    return Categorie.serveur;
                } else if (Categorie.serviceWeb.getValueString().equals(text)) {
                    return Categorie.serviceWeb;
                } else if (Categorie.compteUsager.getValueString().equals(text)) {
                    return Categorie.compteUsager;
                } else if (Categorie.autre.getValueString().equals(text)) {
                    return Categorie.autre;
                }
            }
            return null;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private int id;
    private String sujet;
    private String description;
    private File fichier;
    @OneToOne(fetch = FetchType.EAGER)
    private Utilisateur client;
    @Enumerated(EnumType.STRING)
    private Statut statut;
    @Enumerated(EnumType.STRING)
    private Categorie categorie;
    @OneToMany(fetch = FetchType.EAGER, targetEntity=Commentaire.class, cascade = CascadeType.ALL, mappedBy = "requete")
    private List<Commentaire> commentaires;
    @OneToOne(fetch = FetchType.EAGER)
    private Utilisateur technicien;
    @ManyToOne
    @JoinColumn(name = "utilisateur_id")
    private Utilisateur utilisateur;

    
    
    public Requete() {
        super();
    }

    public Requete(String sujet, String description, Utilisateur client, Categorie categorie)
            throws FileNotFoundException, FileNotFoundException, IOException {
        this.client = client;
        this.sujet = sujet;
        this.description = description;
        this.technicien = client;
        this.statut = Statut.ouvert;
        this.categorie = categorie;
        this.commentaires = new ArrayList<Commentaire>();
    }

    public void finaliser(Statut fin) {
        this.setStatut(fin);
    }

    public void addCommentaire(String note, Utilisateur utilisateur) {
        Commentaire suivant = new Commentaire(note, utilisateur);
        commentaires.add(suivant);
    }

    public void setTech(Utilisateur tech) {
        this.technicien = tech;
        technicien.ajouterRequete(this);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public File getFichier() {
        return fichier;
    }

    public void setFichier(File fichier) {
        this.fichier = fichier;
    }

    public Utilisateur getClient() {
        return client;
    }

    public void setClient(Utilisateur client) {
        this.client = client;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public List<Commentaire> getCommentaires() {
        return commentaires;
    }

    public void setCommentaires(List<Commentaire> listeCommentaires) {
        this.commentaires = listeCommentaires;
    }

    public Utilisateur getTechnicien() {
        return technicien;
    }

    public void setTechnicien(Utilisateur technicien) {
        this.technicien = technicien;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

}