package ca.cranberry.app.service;

import static ca.cranberry.app.utils.IFiles.BANQUE_REQUETES;
import static ca.cranberry.app.utils.IFiles.COMM_ATTRUBUTS_SEPARATEUR;
import static ca.cranberry.app.utils.IFiles.COMM_SEPARATEUR;
import static ca.cranberry.app.utils.IFiles.DIVISEUR;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import ca.cranberry.app.beans.Commentaire;
import ca.cranberry.app.beans.Requete;
import ca.cranberry.app.beans.Utilisateur;
import ca.cranberry.app.database.DatabaseConnection;
import ca.cranberry.app.exception.NomUtilisateurException;

public class BanqueRequetes {

    private static BanqueRequetes instance = null;
    private List<Requete> requetes;

    private BanqueRequetes() throws FileNotFoundException, IOException {
        requetes = new ArrayList<Requete>();

    }

    public static BanqueRequetes getInstance() throws FileNotFoundException, IOException {
        if (instance == null) {
            instance = new BanqueRequetes();
            instance.importerRequetes();
        }
        return instance;
    }

    public void nouvelleRequete(Requete nouvelle, Utilisateur client) throws FileNotFoundException, IOException {
        requetes.add(nouvelle);
        client.ajouterRequete(nouvelle);

    }

    public ArrayList<Requete> getRequetes(Requete.Statut statut) {
        ArrayList<Requete> requetesStatut = new ArrayList<Requete>();
        for (Requete requete : requetesStatut) {
            if (requete.getStatut().equals(statut)) {
                requetesStatut.add(requete);
            }
        }
        return requetesStatut;
    }


    public Requete derniereRequete() {
        return requetes.get(requetes.size() - 1);
    }


    private void importerRequetes() throws FileNotFoundException, IOException {
    	Session session = DatabaseConnection.getSession();
        session.beginTransaction();
        requetes = session.createQuery("from Requete").getResultList();
        session.getTransaction().commit();
        session.close();
    }

    private void nouvelleRequete(Requete nouvelle, String client, String comm)
            throws FileNotFoundException, IOException {

        if (!comm.equals("")) {
            String[] comms = comm.split(COMM_SEPARATEUR);
            for (int i = 0; i < comms.length; i++) {
                String[] ceCommentaire = comms[i].split(COMM_ATTRUBUTS_SEPARATEUR);
                try {
                    nouvelle.addCommentaire(ceCommentaire[0],
                            BanqueUtilisateurs.getInstance().getUtilisateurByNomUtilisateur(ceCommentaire[1]));
                } catch (NomUtilisateurException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        requetes.add(nouvelle);
        nouvelle.getClient().ajouterRequete(nouvelle);

    }

    public void exporterRequetes() throws IOException {
        FileWriter fileWriter = new FileWriter(BANQUE_REQUETES);
        BufferedWriter writer = new BufferedWriter(fileWriter);
        for (Requete requete : requetes) {
            String comments = commentairesToString(requete.getCommentaires());
            final String diviseur = DIVISEUR;
            String path = "";
            String tech = "";
            String categorie = requete.getCategorie().getValueString();
            if (requete.getTechnicien() != null) {
                tech = requete.getTechnicien().getNomUtilisateur();
            }
            if (requete.getFichier() != null) {
                path = requete.getFichier().getPath();
            }
            String requetePlainText = requete.getSujet() + diviseur + requete.getDescription() + diviseur
                    + requete.getClient().getNomUtilisateur() + diviseur + categorie + diviseur
                    + requete.getStatut().getValueString() + diviseur + path + diviseur + tech + diviseur + comments
                    + "\n";

            writer.write(requetePlainText);
        }
        writer.close();

    }

    private String commentairesToString(List<Commentaire> commentaires) {
        String comments = "";
        if (commentaires != null) {
            for (int j = 0; j < commentaires.size(); j++) {
                comments += commentaires.get(j).getNote();
                comments += COMM_ATTRUBUTS_SEPARATEUR + commentaires.get(j).getAuteur().getNomUtilisateur();
                if (commentaires.size() - 1 == j) {
                    comments += "";
                } else {
                    comments += COMM_SEPARATEUR;
                }
            }
        }
        return comments;
    }
}
