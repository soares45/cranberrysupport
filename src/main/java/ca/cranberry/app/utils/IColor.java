package ca.cranberry.app.utils;

import java.awt.Color;

public interface IColor {
	final static Color COMM_BACKGROUND = new Color(102, 153, 255);
	final static Color DEFAULT_BACKGROUND = new Color(255, 255, 255);
}
