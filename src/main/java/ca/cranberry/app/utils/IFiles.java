package ca.cranberry.app.utils;

public interface IFiles {
	static final String BANQUE_REQUETES = "dat/ca.cranberry.app.service.BanqueRequetes.txt";
	static final String BANQUE_UTILISATEURS = "dat/BanqueUtilisateur.txt";
	static final String DATA_FOLDER = "dat/";
	static final String DIVISEUR = ";~";
	static final String COMM_SEPARATEUR = "/%";
	static final String COMM_ATTRUBUTS_SEPARATEUR = "&#";
	static final String UTIL_SEPARATOR = ";";
	
}
