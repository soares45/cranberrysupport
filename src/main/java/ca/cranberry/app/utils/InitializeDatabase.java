package ca.cranberry.app.utils;

import org.hibernate.Session;

import ca.cranberry.app.beans.Utilisateur;
import ca.cranberry.app.database.DatabaseConnection;

public class InitializeDatabase {


    public static void genereClient(){
        Utilisateur client1 = new Utilisateur("Charle", "Xavier", "RealXMen224", "diabloisabitch", Utilisateur.Role.client);
        Utilisateur client2 = new Utilisateur("Elon", "Musk", "Electro", "gasisforpusies", Utilisateur
                .Role.client);
        Utilisateur client3 = new Utilisateur("Barry", "Allen", "notTheFlash", "IrisWest", Utilisateur
                .Role.client);

        Session session = DatabaseConnection.getSession();
        session.beginTransaction();
        session.save(client1);
        session.save(client2);
        session.save(client3);
        session.getTransaction().commit();
        session.close();
    }

    public static void genereTech(){

        Utilisateur tech1 = new Utilisateur("Charle", "Darwin", "apple", "gravityisabitch", Utilisateur.Role
                .technicien);
        Utilisateur tech2 = new Utilisateur("kirigaya", "kazuto", "kirito", "saowasnotthatbad", Utilisateur
                .Role.technicien);
        Utilisateur tech3 = new Utilisateur("morgan", "freeman", "god", "password", Utilisateur
                .Role.technicien);
        Session session = DatabaseConnection.getSession();
        session.beginTransaction();
        session.save(tech1);
        session.save(tech2);
        session.save(tech3);
        session.getTransaction().commit();
        session.close();
    }

    public static void main(String args[]){

        genereClient();
        genereTech();
    }
}
