package ca.cranberry.app.view;

import static ca.cranberry.app.utils.IFiles.DATA_FOLDER;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_CATEGORIE_BOX_GAP_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_CATEGORIE_GAP_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_COMPLETE_BUTTON_GAP_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_DESCRIPTION_GAP_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_FIELD_SIZE_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_FORM_GAP_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_QUITTER_BUTTON_GAP_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_SEPARATOR_SIZE_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.VERTICAL_CATEGORIE_GAP_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.VERTICAL_DESCRIPTION_SIZE_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.VERTICAL_SEL_PATH_BUTTON_GAP_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.VERTICAL_SEPARATOR_SIZE_NOUV_FEN;
import static ca.cranberry.app.utils.ISize.VERTICAL_TITRE_GAP_NOUV_FEN;
import static ca.cranberry.app.utils.IView.CLIENT;
import static ca.cranberry.app.utils.IView.INITIAL_CATEGORIE;
import static ca.cranberry.app.utils.IView.LABEL_CATEGORIE;
import static ca.cranberry.app.utils.IView.LABEL_DESCRIPTION;
import static ca.cranberry.app.utils.IView.LABEL_FICHIER;
import static ca.cranberry.app.utils.IView.LABEL_NOUVELLE_REQUETE;
import static ca.cranberry.app.utils.IView.LABEL_REVENIR;
import static ca.cranberry.app.utils.IView.LABEL_SUJET_REQUETE;
import static ca.cranberry.app.utils.IView.LABEL_TELEVERSE_FILE;
import static ca.cranberry.app.utils.IView.LABEL_TERMINER;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

import ca.cranberry.app.beans.Requete;
import ca.cranberry.app.beans.Utilisateur;
import ca.cranberry.app.service.BanqueRequetes;

public class FenetreNouvelleRequete extends JFrame {

    private static final long serialVersionUID = -8553551926775617152L;
    private JComboBox<String> CategorieBox;
    private JLabel categorieJLabel;
    private JTextArea descriptionZone;
    private JScrollPane descriptionJScroll;
    private JLabel descriptionJLabel;
    private JButton completerButton;
    private JLabel fichierJLabel;
    private JFileChooser fichierChooser;
    private JSeparator jSeparator;
    private JTextField pathFld;
    private JButton quitterButton;
    private JButton selPathBtn;
    private JTextField sujetField;
    private JLabel sujetJLabel;
    private JLabel titreJLabel;
    private Utilisateur utilsateur;
    private JFrame framePrecedant;
    private File file;

    public FenetreNouvelleRequete(Utilisateur lui, JFrame pagePrecedente) {
        initializeTitre();
        initializeSujet();
        initializeDescription();
        initializeFichier();
        initializePath();
        initializeSelPath();
        initializeCategorie();
        initializeCompleter();
        initializeQuitter();
        initComponents();
        this.setVisible(true);
        utilsateur = lui;
        framePrecedant = pagePrecedente;

    }

    private void initComponents() {

        jSeparator = new JSeparator();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layoutSetHorizontal(layout);
        layoutSetVertical(layout);

        pack();
    }

    private void initializeQuitter() {
        quitterButton = new JButton();
        quitterButton.setText(LABEL_REVENIR);
        quitterButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                quitBtnActionPerformed(evt);
            }
        });
    }

    private void initializeCompleter() {
        completerButton = new JButton();
        completerButton.setText(LABEL_TERMINER);
        completerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                doneBtnActionPerformed(evt);
            }
        });
    }

    private void initializeCategorie() {
        CategorieBox = new JComboBox<String>();
        categorieJLabel = new JLabel();
        CategorieBox.setModel(new DefaultComboBoxModel<>(INITIAL_CATEGORIE));
        categorieJLabel.setText(LABEL_CATEGORIE);
    }

    private void initializeSelPath() {
        selPathBtn = new JButton();
        selPathBtn.setText(LABEL_TELEVERSE_FILE);
        selPathBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                selPathBtnActionPerformed(evt);
            }
        });
    }

    private void initializePath() {
        pathFld = new JTextField();
        pathFld.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                pathFldActionPerformed(evt);
            }
        });
    }

    private void initializeFichier() {
        fichierJLabel = new JLabel();
        fichierJLabel.setText(LABEL_FICHIER);
    }

    private void initializeDescription() {
        descriptionJLabel = new JLabel();
        descriptionJLabel.setText(LABEL_DESCRIPTION);

        descriptionZone = new JTextArea();
        descriptionZone.setColumns(20);
        descriptionZone.setRows(5);
        descriptionZone.setBorder(null);
        descriptionJScroll = new JScrollPane();
        descriptionJScroll.setViewportView(descriptionZone);
    }

    private void initializeSujet() {
        sujetJLabel = new JLabel();
        sujetJLabel.setText(LABEL_SUJET_REQUETE);

        sujetField = new JTextField();
    }

    private void initializeTitre() {
        titreJLabel = new JLabel();
        titreJLabel.setText(LABEL_NOUVELLE_REQUETE);
    }

    private void layoutSetHorizontal(GroupLayout layout) {
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup().addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
                        .createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(titreJLabel)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                .addComponent(sujetJLabel).addComponent(descriptionJLabel)
                                                .addComponent(fichierJLabel))
                                        .addGap(HORIZONTAL_DESCRIPTION_GAP_NOUV_FEN, HORIZONTAL_DESCRIPTION_GAP_NOUV_FEN, HORIZONTAL_DESCRIPTION_GAP_NOUV_FEN)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                .addComponent(pathFld, GroupLayout.DEFAULT_SIZE, HORIZONTAL_FIELD_SIZE_NOUV_FEN, Short.MAX_VALUE)
                                                .addComponent(sujetField, GroupLayout.DEFAULT_SIZE, HORIZONTAL_FIELD_SIZE_NOUV_FEN,
                                                        Short.MAX_VALUE)
                                                .addComponent(descriptionJScroll, GroupLayout.DEFAULT_SIZE, HORIZONTAL_FIELD_SIZE_NOUV_FEN,
                                                        Short.MAX_VALUE)
                                                .addComponent(selPathBtn))))
                        .addContainerGap(HORIZONTAL_FORM_GAP_NOUV_FEN, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup().addComponent(categorieJLabel).addGap(HORIZONTAL_CATEGORIE_GAP_NOUV_FEN, HORIZONTAL_CATEGORIE_GAP_NOUV_FEN, HORIZONTAL_CATEGORIE_GAP_NOUV_FEN)
                                .addComponent(CategorieBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                        GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(HORIZONTAL_CATEGORIE_BOX_GAP_NOUV_FEN, Short.MAX_VALUE))
                        .addGroup(GroupLayout.Alignment.TRAILING,
                                layout.createSequentialGroup().addGap(HORIZONTAL_COMPLETE_BUTTON_GAP_NOUV_FEN, HORIZONTAL_COMPLETE_BUTTON_GAP_NOUV_FEN, HORIZONTAL_COMPLETE_BUTTON_GAP_NOUV_FEN).addComponent(completerButton)
                                        .addGap(HORIZONTAL_QUITTER_BUTTON_GAP_NOUV_FEN, HORIZONTAL_QUITTER_BUTTON_GAP_NOUV_FEN, HORIZONTAL_QUITTER_BUTTON_GAP_NOUV_FEN).addComponent(quitterButton)
                                        .addContainerGap(104, Short.MAX_VALUE))))
                .addComponent(jSeparator, GroupLayout.DEFAULT_SIZE, HORIZONTAL_SEPARATOR_SIZE_NOUV_FEN, Short.MAX_VALUE));
    }

    private void layoutSetVertical(GroupLayout layout) {
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup().addContainerGap().addComponent(titreJLabel).addGap(VERTICAL_TITRE_GAP_NOUV_FEN, VERTICAL_TITRE_GAP_NOUV_FEN, VERTICAL_TITRE_GAP_NOUV_FEN)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(sujetJLabel)
                        .addComponent(sujetField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(descriptionJLabel)
                        .addComponent(descriptionJScroll, GroupLayout.PREFERRED_SIZE, VERTICAL_DESCRIPTION_SIZE_NOUV_FEN, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(pathFld, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                GroupLayout.PREFERRED_SIZE)
                        .addComponent(fichierJLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(selPathBtn).addGap(VERTICAL_SEL_PATH_BUTTON_GAP_NOUV_FEN, VERTICAL_SEL_PATH_BUTTON_GAP_NOUV_FEN, VERTICAL_SEL_PATH_BUTTON_GAP_NOUV_FEN)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(CategorieBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                GroupLayout.PREFERRED_SIZE)
                        .addComponent(categorieJLabel))
                .addGap(VERTICAL_CATEGORIE_GAP_NOUV_FEN, VERTICAL_CATEGORIE_GAP_NOUV_FEN, VERTICAL_CATEGORIE_GAP_NOUV_FEN).addComponent(jSeparator, GroupLayout.PREFERRED_SIZE, VERTICAL_SEPARATOR_SIZE_NOUV_FEN, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(quitterButton)
                        .addComponent(completerButton))
                .addContainerGap(31, Short.MAX_VALUE)));
    }

    private void pathFldActionPerformed(ActionEvent evt) {// GEN-FIRST:event_pathFldActionPerformed
        pathFld.setText(fichierChooser.getSelectedFile().getPath());
    }

    private void selPathBtnActionPerformed(ActionEvent evt) {// GEN-FIRST:event_selPathBtnActionPerformed
        fichierChooser.showOpenDialog(framePrecedant);

        pathFldActionPerformed(evt);
        try {
            file = new File(DATA_FOLDER + fichierChooser.getSelectedFile().getName());
            InputStream srcFile = new FileInputStream(fichierChooser.getSelectedFile());
            OutputStream newFile = new FileOutputStream(file);
            byte[] buf = new byte[4096];
            int len;
            while ((len = srcFile.read(buf)) > 0) {
                newFile.write(buf, 0, len);
            }
            srcFile.close();
            newFile.close();
        } catch (IOException ex) {
            Logger.getLogger(FenetreNouvelleRequete.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void doneBtnActionPerformed(ActionEvent evt) {
        try {
            Requete nouvelle = new Requete(sujetField.getText(), descriptionZone.getText().replaceAll("\n", " "),
                    utilsateur, Requete.Categorie.fromString((String) CategorieBox.getSelectedItem()));
            BanqueRequetes.getInstance().nouvelleRequete(nouvelle, utilsateur);
            BanqueRequetes.getInstance().derniereRequete().setFichier(file);

            this.setVisible(false);
            if (utilsateur.getRole().equals(CLIENT)) {
                FenetreClientConnection retourPageClient = new FenetreClientConnection(utilsateur);
                retourPageClient.setVisible(true);
            } else {
                FenetreTechnicien retourPageTech = new FenetreTechnicien(utilsateur);
                retourPageTech.setVisible(true);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FenetreNouvelleRequete.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FenetreNouvelleRequete.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void quitBtnActionPerformed(ActionEvent evt) {
        this.setVisible(false);
        framePrecedant.setVisible(true);

    }

}
