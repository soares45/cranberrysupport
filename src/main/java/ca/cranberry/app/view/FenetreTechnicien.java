package ca.cranberry.app.view;

import static ca.cranberry.app.utils.IFiles.DATA_FOLDER;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_CATEGORIE_BOX_SIZE_FEN_TECH;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_COMM_SIZE_FEN_TECH;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_PRENDRE_BUTTON_GAP_FEN_TECH;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_REQUETES_ASSIGNED_CONTAINER_GAP_FEN_TECH;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_REQUETES_ASSIGNED_LABEL_GAP_FEN_TECH;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_REQUETES_ASSIGNED_LABEL_SIZE_FEN_TECH;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_REQUETES_ASSIGNED_SIZE_FEN_TECH;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_REQUETES_DISPO_CONTAINER_GAP_FEN_TECH;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_REQUETES_DISPO_GAP_FEN_TECH;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_REQUETES_DISPO_SIZE_FEN_TECH;
import static ca.cranberry.app.utils.ISize.REQUETE_COLUMN;
import static ca.cranberry.app.utils.ISize.REQUETE_ROW;
import static ca.cranberry.app.utils.ISize.VERTICAL_ADD_REQUETE_ASSIGNEE_SIZE_FEN_TECH;
import static ca.cranberry.app.utils.ISize.VERTICAL_ADD_REQUETE_BUTTON_GAP_FEN_TECH;
import static ca.cranberry.app.utils.ISize.VERTICAL_ASSIGNEMENT_GAP_FEN_TECH;
import static ca.cranberry.app.utils.ISize.VERTICAL_DRESC_REQUETES_DISPO_SIZE_FEN_TECH;
import static ca.cranberry.app.utils.ISize.VERTICAL_REQUETES_DISPO_GAP_FEN_TECH;
import static ca.cranberry.app.utils.ISize.VERTICAL_REQUETES_DISPO_SIZE_FEN_TECH;
import static ca.cranberry.app.utils.ISize.VERTICAL_SUPPORT_REQUETES_DISPO_SIZE_FEN_TECH;
import static ca.cranberry.app.utils.IView.INITIAL_CATEGORIE_TECH;
import static ca.cranberry.app.utils.IView.LABEL_ADD_FILE;
import static ca.cranberry.app.utils.IView.LABEL_AJOUTER_COMM;
import static ca.cranberry.app.utils.IView.LABEL_ASSIGNED_REQUETE;
import static ca.cranberry.app.utils.IView.LABEL_ASSIGN_REQUETE;
import static ca.cranberry.app.utils.IView.LABEL_COMM_INDEX;
import static ca.cranberry.app.utils.IView.LABEL_CREATE_REQUETE;
import static ca.cranberry.app.utils.IView.LABEL_NO_ATTACHED_FILES;
import static ca.cranberry.app.utils.IView.LABEL_NO_REQUETE;
import static ca.cranberry.app.utils.IView.LABEL_NO_TECH;
import static ca.cranberry.app.utils.IView.LABEL_QUIT;
import static ca.cranberry.app.utils.IView.LABEL_REQUETES_EMPTY;
import static ca.cranberry.app.utils.IView.LABEL_REQUETE_NON_ASSIGN_INDEX;
import static ca.cranberry.app.utils.IView.LABEL_REQUETE_SEL;
import static ca.cranberry.app.utils.IView.LABEL_SHOW_RAPPORT;
import static ca.cranberry.app.utils.IView.LABEL_TAKE_REQUETE_SEL;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ca.cranberry.app.beans.Commentaire;
import ca.cranberry.app.beans.Requete;
import ca.cranberry.app.beans.Utilisateur;
import ca.cranberry.app.service.BanqueRequetes;
import ca.cranberry.app.service.BanqueUtilisateurs;


public class FenetreTechnicien extends JFrame {

    private static final long serialVersionUID = -396905215857647505L;
    private Utilisateur utilisateur;
    private List<Requete> requetesTechAssigner;
    private List<Requete> requetesDisponible;
    private List<Utilisateur> techniciens;
    private Requete requeteToShow;
    private JFrame fenetrePrecedante;
    private File fichier;
    private boolean isShows = false;
    private DefaultListModel<String> requetesAssignerModel = new DefaultListModel<String>();
    private DefaultListModel<String> requetesDisponibleModel = new DefaultListModel<String>();
    private DefaultListModel<String> techniciensModel = new DefaultListModel<String>();
    private JLabel ajouterCommantaireLabel;
    private JButton afficheRapportButton;
    private JButton ajouterFichierButton;
    private JButton assignerRequeteButton;
    private JComboBox<String> categorieBox;
    private JTextField commantaireField;
    private JLabel commantaireLabel;
    private JTextArea commentaireZone;
    private JTextArea descriptionRequeteZone;
    private JFileChooser fichierChooser;
    private JLabel fichierLabel;
    private JComboBox<String> finaliserBox;
    private JScrollPane requetesAssigneeScroll;
    private JScrollPane requeteScroll;
    private JScrollPane commentaireScroll;
    private JScrollPane requetesDispoScoll;
    private JScrollPane descriptionRequeteScroll;
    private JScrollPane supportScroll;
    private JButton nouvelleRequeteButton;
    private JButton prendreRequeteButton;
    private JButton quitterButton;
    private JLabel requeteAssignerLabel;
    private JList<String> requetesAssignee;
    private JList<String> requetesDispo;
    private JLabel requeteDispoLabel;
    private JLabel requeteSelLabel;
    private JTextArea requeteZone;
    private JList<String> supportList;

    public FenetreTechnicien(Utilisateur tech) throws FileNotFoundException, IOException {
        initializeRequeteAssigner();
        initializeRequete();
        initializeCommantaire();
        initializeCategorie();
        initializeRequeteDispo();
        initializePrendreRequete();
        initializeDescription();
        initializeNouvelleRequete();
        initializeAssignerRequeteButton();
        initializeSupport();
        initializeFinaliser();
        initializeFichier();
        initializeRapport();
        initializeQuitter();
        initializeFichierLabel();
        initComponents();
        this.setVisible(true);
        utilisateur = tech;
        requetesTechAssigner = tech.getRequetes();
        requetesDisponible = BanqueRequetes.getInstance().getRequetes(Requete.Statut.ouvert);
        techniciens = BanqueUtilisateurs.getInstance().getUtilisateurs("technicien");

        initializeModel();

    }

    private void initComponents() {

        fichierChooser = new JFileChooser();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layoutSetHorizontal(layout);
        layoutSetVertical(layout);

        pack();
    }

    private void initializeFichierLabel() {
        fichierLabel = new JLabel();
        fichierLabel.setText(LABEL_NO_ATTACHED_FILES);
        fichierLabel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                fileLblMouseClicked(evt);
            }
        });
    }

    private void initializeQuitter() {
        quitterButton = new JButton();
        quitterButton.setText(LABEL_QUIT);
        quitterButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                quitBtnActionPerformed(evt);
            }
        });
    }

    private void initializeRapport() {
        afficheRapportButton = new JButton();
        afficheRapportButton.setText(LABEL_SHOW_RAPPORT);
        afficheRapportButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                affRapportBtnActionPerformed(evt);
            }
        });
    }

    private void initializeFichier() {
        ajouterFichierButton = new JButton();
        ajouterFichierButton.setText(LABEL_ADD_FILE);
        ajouterFichierButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ajoutfichierBtnActionPerformed(evt);
            }
        });
    }

    private void initializeFinaliser() {
        finaliserBox = new JComboBox<String>();
        finaliserBox.setModel(new DefaultComboBoxModel<>(new String[] { "Finaliser", "Succès", "Abandon", " " }));
        finaliserBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                finBoxItemStateChanged(evt);
            }
        });
    }

    private void initializeSupport() {
        supportScroll = new JScrollPane();
        supportList = new JList<String>();
        supportScroll.setViewportView(supportList);
    }

    private void initializeAssignerRequeteButton() {
        assignerRequeteButton = new JButton();
        assignerRequeteButton.setText(LABEL_ASSIGN_REQUETE);
        assignerRequeteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                assignerReqBtnActionPerformed(evt);
            }
        });
    }

    private void initializeNouvelleRequete() {
        nouvelleRequeteButton = new JButton();
        nouvelleRequeteButton.setText(LABEL_CREATE_REQUETE);
        nouvelleRequeteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                newRequeteBtnActionPerformed(evt);
            }
        });
    }

    private void initializeDescription() {
        descriptionRequeteScroll = new JScrollPane();
        descriptionRequeteZone = new JTextArea();
        descriptionRequeteZone.setColumns(20);
        descriptionRequeteZone.setRows(5);
        descriptionRequeteScroll.setViewportView(descriptionRequeteZone);
    }

    private void initializePrendreRequete() {
        prendreRequeteButton = new JButton();
        prendreRequeteButton.setText(LABEL_TAKE_REQUETE_SEL);
        prendreRequeteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                prendrereqBtnActionPerformed(evt);
            }
        });
    }

    private void initializeRequeteDispo() {
        requeteDispoLabel = new JLabel();
        requetesDispoScoll = new JScrollPane();
        requetesDispo = new JList<String>();
        requeteDispoLabel.setText(LABEL_REQUETE_NON_ASSIGN_INDEX);

        requetesDispo.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent evt) {
                reqDispoValueChanged(evt);
            }
        });
        requetesDispoScoll.setViewportView(requetesDispo);
    }

    private void initializeCategorie() {
        categorieBox = new JComboBox<String>();
        categorieBox.setModel(new DefaultComboBoxModel<String>(INITIAL_CATEGORIE_TECH));
        categorieBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                catBoxItemStateChanged(evt);
            }
        });
    }

    private void initializeCommantaire() {
        commantaireLabel = new JLabel();
        commentaireScroll = new JScrollPane();
        commentaireZone = new JTextArea();
        ajouterCommantaireLabel = new JLabel();
        commantaireField = new JTextField();
        commantaireLabel.setText(LABEL_COMM_INDEX);

        commentaireZone.setColumns(20);
        commentaireZone.setRows(5);
        commentaireScroll.setViewportView(commentaireZone);

        ajouterCommantaireLabel.setText(LABEL_AJOUTER_COMM);

        commantaireField.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent evt) {
                comInKeyPressed(evt);
            }
        });
    }

    private void initializeRequete() {
        requeteSelLabel = new JLabel();
        requeteScroll = new JScrollPane();
        requeteZone = new JTextArea();
        requeteSelLabel.setText(LABEL_REQUETE_SEL);

        requeteZone.setColumns(REQUETE_COLUMN);
        requeteZone.setRows(REQUETE_ROW);
        requeteScroll.setViewportView(requeteZone);
    }

    private void initializeRequeteAssigner() {
        requeteAssignerLabel = new JLabel();
        requetesAssigneeScroll = new JScrollPane();
        requetesAssignee = new JList<String>();
        requeteAssignerLabel.setText(LABEL_ASSIGNED_REQUETE);
        requetesAssignee.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent evt) {
                reqAssigneeValueChanged(evt);
            }
        });
        requetesAssigneeScroll.setViewportView(requetesAssignee);
    }

    private void layoutSetHorizontal(GroupLayout layout) {
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
                GroupLayout.Alignment.TRAILING,
                layout.createSequentialGroup().addContainerGap().addGroup(layout
                        .createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup().addGroup(layout
                                .createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(requeteDispoLabel)
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(requetesDispoScoll, GroupLayout.PREFERRED_SIZE, HORIZONTAL_REQUETES_DISPO_SIZE_FEN_TECH,
                                                GroupLayout.PREFERRED_SIZE)
                                        .addGap(HORIZONTAL_REQUETES_DISPO_GAP_FEN_TECH, HORIZONTAL_REQUETES_DISPO_GAP_FEN_TECH, HORIZONTAL_REQUETES_DISPO_GAP_FEN_TECH)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                .addComponent(descriptionRequeteScroll)
                                                .addComponent(prendreRequeteButton))
                                        .addGap(HORIZONTAL_PRENDRE_BUTTON_GAP_FEN_TECH, HORIZONTAL_PRENDRE_BUTTON_GAP_FEN_TECH, HORIZONTAL_PRENDRE_BUTTON_GAP_FEN_TECH)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(supportScroll, GroupLayout.Alignment.LEADING, 0, 0,
                                                        Short.MAX_VALUE)
                                                .addComponent(assignerRequeteButton, GroupLayout.Alignment.LEADING))))
                                .addGap(HORIZONTAL_REQUETES_DISPO_CONTAINER_GAP_FEN_TECH, HORIZONTAL_REQUETES_DISPO_CONTAINER_GAP_FEN_TECH, HORIZONTAL_REQUETES_DISPO_CONTAINER_GAP_FEN_TECH))
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(requetesAssigneeScroll, GroupLayout.DEFAULT_SIZE, HORIZONTAL_REQUETES_ASSIGNED_SIZE_FEN_TECH,
                                                Short.MAX_VALUE)
                                        .addComponent(requeteAssignerLabel, GroupLayout.Alignment.TRAILING,
                                                GroupLayout.DEFAULT_SIZE, HORIZONTAL_REQUETES_ASSIGNED_LABEL_SIZE_FEN_TECH, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(requeteSelLabel)
                                        .addComponent(requeteScroll, GroupLayout.PREFERRED_SIZE,
                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(categorieBox, GroupLayout.PREFERRED_SIZE, HORIZONTAL_CATEGORIE_BOX_SIZE_FEN_TECH,
                                                GroupLayout.PREFERRED_SIZE)
                                        .addComponent(finaliserBox, GroupLayout.PREFERRED_SIZE,
                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGap(HORIZONTAL_REQUETES_ASSIGNED_LABEL_GAP_FEN_TECH,HORIZONTAL_REQUETES_ASSIGNED_LABEL_GAP_FEN_TECH,HORIZONTAL_REQUETES_ASSIGNED_LABEL_GAP_FEN_TECH)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
                                        .createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(commentaireScroll, GroupLayout.PREFERRED_SIZE, HORIZONTAL_COMM_SIZE_FEN_TECH,
                                                GroupLayout.PREFERRED_SIZE)
                                        .addComponent(commantaireLabel)
                                        .addGroup(layout.createSequentialGroup().addComponent(ajouterCommantaireLabel)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(commantaireField)))
                                        .addGroup(layout.createSequentialGroup().addComponent(ajouterFichierButton)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(fichierLabel)))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, HORIZONTAL_REQUETES_ASSIGNED_CONTAINER_GAP_FEN_TECH, Short.MAX_VALUE)))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(quitterButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE)
                                .addComponent(afficheRapportButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE)
                                .addComponent(nouvelleRequeteButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE))
                        .addContainerGap()));
    }

    private void layoutSetVertical(GroupLayout layout) {
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup().addGap(VERTICAL_ADD_REQUETE_BUTTON_GAP_FEN_TECH, VERTICAL_ADD_REQUETE_BUTTON_GAP_FEN_TECH, VERTICAL_ADD_REQUETE_BUTTON_GAP_FEN_TECH).addComponent(nouvelleRequeteButton)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(afficheRapportButton).addPreferredGap(
                                        LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(quitterButton))
                        .addGroup(layout.createSequentialGroup().addContainerGap().addComponent(requeteAssignerLabel)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(requetesAssigneeScroll, GroupLayout.DEFAULT_SIZE, VERTICAL_ADD_REQUETE_ASSIGNEE_SIZE_FEN_TECH,
                                                Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                        .addComponent(commantaireLabel)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(commentaireScroll, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                        .addGroup(layout
                                                                .createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                                .addComponent(ajouterCommantaireLabel).addComponent(
                                                                        commantaireField, GroupLayout.PREFERRED_SIZE,
                                                                        GroupLayout.DEFAULT_SIZE,
                                                                        GroupLayout.PREFERRED_SIZE))
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
                                                                GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addGroup(layout
                                                                .createParallelGroup(GroupLayout.Alignment.BASELINE)
                                                                .addComponent(ajouterFichierButton)
                                                                .addComponent(fichierLabel)))
                                                .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                        .addComponent(requeteSelLabel)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(requeteScroll, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(categorieBox, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(finaliserBox, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                                .addGap(VERTICAL_ASSIGNEMENT_GAP_FEN_TECH,VERTICAL_ASSIGNEMENT_GAP_FEN_TECH,VERTICAL_ASSIGNEMENT_GAP_FEN_TECH)))
                .addGap(VERTICAL_REQUETES_DISPO_GAP_FEN_TECH,VERTICAL_REQUETES_DISPO_GAP_FEN_TECH,VERTICAL_REQUETES_DISPO_GAP_FEN_TECH).addComponent(requeteDispoLabel)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(requetesDispoScoll, GroupLayout.PREFERRED_SIZE, VERTICAL_REQUETES_DISPO_SIZE_FEN_TECH, GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup().addComponent(prendreRequeteButton)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(descriptionRequeteScroll, GroupLayout.DEFAULT_SIZE, VERTICAL_DRESC_REQUETES_DISPO_SIZE_FEN_TECH, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup().addComponent(assignerRequeteButton)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(supportScroll, GroupLayout.DEFAULT_SIZE, VERTICAL_SUPPORT_REQUETES_DISPO_SIZE_FEN_TECH, Short.MAX_VALUE)))
                .addContainerGap()));
    }

    private void initializeModel() {
        if (requetesTechAssigner.isEmpty()) {
            requetesAssignerModel.addElement(LABEL_REQUETES_EMPTY);
            requetesAssignee.setModel(requetesAssignerModel);
        } else {
            for (int i = 0; i < requetesTechAssigner.size(); i++) {
                requetesAssignerModel.addElement(requetesTechAssigner.get(i).getSujet());
            }
            requetesAssignee.setModel(requetesAssignerModel);
        }

        if (requetesDisponible.isEmpty()) {
            requetesDisponibleModel.addElement(LABEL_NO_REQUETE);
            requetesDispo.setModel(requetesDisponibleModel);
            prendreRequeteButton.setEnabled(false);
        } else {
            for (int i = 0; i < requetesDisponible.size(); i++) {
                requetesDisponibleModel.addElement(requetesDisponible.get(i).getSujet());
            }
            requetesDispo.setModel(requetesDisponibleModel);
            prendreRequeteButton.setEnabled(true);

        }

        if (techniciens.isEmpty()) {
            techniciensModel.addElement(LABEL_NO_TECH);
            supportList.setModel(techniciensModel);
        } else {
            for (int i = 0; i < techniciens.size(); i++) {
                techniciensModel.addElement(techniciens.get(i).getNomUtilisateur());
            }
            supportList.setModel(techniciensModel);

        }
    }

    private void quitBtnActionPerformed(ActionEvent evt) {
        System.exit(0);
    }

    private void reqAssigneeValueChanged(ListSelectionEvent evt) {
        updateRequeteA();
        updateCommentaires();
    }

    private void reqDispoValueChanged(ListSelectionEvent evt) {
        if (!requetesDisponible.isEmpty()) {
            updateRequeteD();
        }
    }

    private void catBoxItemStateChanged(ItemEvent evt) {
        if (categorieBox.getSelectedIndex() > 0) {
            requetesTechAssigner.get(requetesAssignee.getSelectedIndex())
                    .setCategorie(Requete.Categorie.fromString((String) categorieBox.getSelectedItem()));
            updateRequeteA();
        }
    }

    private void prendrereqBtnActionPerformed(ActionEvent evt) {
        if (requetesDispo.getSelectedIndex() >= 0) {
            Requete r = requetesDisponible.get(requetesDispo.getSelectedIndex());
            r.setTech(utilisateur);
            r.setStatut(Requete.Statut.enTraitement);

            requetesTechAssigner = utilisateur.getRequetes();

            requetesAssignerModel = new DefaultListModel<String>();
            for (int i = 0; i < requetesTechAssigner.size(); i++) {
                requetesAssignerModel.addElement(requetesTechAssigner.get(i).getSujet());
            }
            requetesAssignee.setModel(requetesAssignerModel);

            requetesDisponibleModel.remove(requetesDispo.getSelectedIndex());
            requetesDisponible.remove(r);

            updateTables();
        }

    }

    private void comInKeyPressed(KeyEvent evt) {
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            requeteToShow.addCommentaire(commantaireField.getText(), utilisateur);
            String s = "";
            for (Commentaire cmt : requeteToShow.getCommentaires()) {
                s += cmt.toString();
            }
            commentaireZone.setText(s);
            commantaireField.setText("");
        }
    }

    private void ajoutfichierBtnActionPerformed(ActionEvent evt) {
        fichierChooser.showOpenDialog(fenetrePrecedante);

        File f = fichierChooser.getSelectedFile();
        if (f != null) {
            try {
                fichier = new File(DATA_FOLDER + f.getName());
                InputStream srcFile = new FileInputStream(f);
                OutputStream newFile = new FileOutputStream(fichier);
                byte[] buf = new byte[4096];
                int len;
                while ((len = srcFile.read(buf)) > 0) {
                    newFile.write(buf, 0, len);
                }
                srcFile.close();
                newFile.close();
            } catch (IOException ex) {
                Logger.getLogger(FenetreNouvelleRequete.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (fichier.exists()) {
                FenetreAlerte ok = new FenetreAlerte();
                ok.setVisible(true);
            }
        }

        try {
            requetesTechAssigner.get(requetesAssignee.getSelectedIndex()).setFichier(fichier);
        } catch (IndexOutOfBoundsException e) {
        }

        updateRequeteA();
    }

    private void fileLblMouseClicked(MouseEvent evt) {
        try {
            Requete req = requetesTechAssigner.get(requetesAssignee.getSelectedIndex());
            java.awt.Desktop dt = java.awt.Desktop.getDesktop();
            dt.open(req.getFichier());
        } catch (IOException ex) {
            Logger.getLogger(FenetreTechnicien.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void finBoxItemStateChanged(ItemEvent evt) {
        if (finaliserBox.getSelectedIndex() > 0) {
            requetesTechAssigner.get(requetesAssignee.getSelectedIndex())
                    .setStatut(Requete.Statut.fromString((String) finaliserBox.getSelectedItem()));
            updateRequeteA();
        }
    }

    private void assignerReqBtnActionPerformed(ActionEvent evt) {
        if (requetesDispo.getSelectedIndex() >= 0) {
            Requete r = requetesDisponible.get(requetesDispo.getSelectedIndex());
            Utilisateur autre = techniciens.get(supportList.getSelectedIndex());
            r.setTech(autre);
            r.setStatut(Requete.Statut.enTraitement);

            requetesDisponibleModel.remove(requetesDispo.getSelectedIndex());
            requetesDisponible.remove(r);

            updateTables();
        }
    }

    private void newRequeteBtnActionPerformed(ActionEvent evt) {
        this.setVisible(false);
        FenetreNouvelleRequete nouvelleRequete = new FenetreNouvelleRequete(utilisateur, this);

    }

    private void affRapportBtnActionPerformed(ActionEvent evt) {

        String rap = "";
        Utilisateur tempo;
        for (int i = 0; i < techniciens.size(); i++) {
            rap += techniciens.get(i).getNom() + ":\n";
            tempo = techniciens.get(i);
            rap += tempo.getRequeteParStatut() + "\n";

        }
        FenetreRapport rapport = new FenetreRapport(rap);
        rapport.setVisible(true);

    }

    private void updateRequeteA() {
        if (requetesAssignee.getSelectedIndex() >= 0) {
            requeteToShow = requetesTechAssigner.get(requetesAssignee.getSelectedIndex());
            requeteZone.setText("Sujet: " + requeteToShow.getSujet() + "\nDescription: "
                    + requeteToShow.getDescription() + "\nCatégorie: " + requeteToShow.getCategorie().toString()
                    + "\nStatut: " + requeteToShow.getStatut().toString());
            String s = "";
            for (Commentaire cmt : requeteToShow.getCommentaires()) {
                s += cmt.toString();
            }
            commentaireZone.setText(s);
            if (requeteToShow.getFichier() != null && requeteToShow.getFichier().exists()) {
                fichierLabel.setVisible(true);
                fichierLabel.setText("Afficher " + requeteToShow.getFichier().getPath());
            } else {
                fichierLabel.setVisible(false);
            }
        } else {
            requeteZone.setText("");
        }
        if (requeteToShow.getStatut() == Requete.Statut.enTraitement) {
            finaliserBox.setEnabled(true);
        } else {
            finaliserBox.setEnabled(false);
        }
    }

    private void updateRequeteD() {
        if (requetesDispo.getSelectedIndex() >= 0) {
            requeteToShow = requetesDisponible.get(requetesDispo.getSelectedIndex());
            descriptionRequeteZone.setText("Sujet: " + requeteToShow.getSujet() + "\nDescription: "
                    + requeteToShow.getDescription() + "\nCatégorie: " + requeteToShow.getCategorie().toString()
                    + "\nStatut: " + requeteToShow.getStatut().toString());
        } else {
            descriptionRequeteZone.setText("");
        }
    }

    private void updateCommentaires() {
        if (requeteToShow != null) {
            if (!isShows) {
                String s = "";
                for (Commentaire c : requeteToShow.getCommentaires()) {
                    s += c.getAuteur().getNomUtilisateur() + ": " + c.getNote() + "\n";

                }
                commentaireZone.setText(s);
                isShows = true;

            }

        }
    }

    private void updateTables() {
        requetesAssignee.setModel(requetesAssignerModel);
        if (!requetesDisponibleModel.isEmpty()) {
            requetesDispo.setModel(requetesDisponibleModel);
            prendreRequeteButton.setEnabled(true);
        } else {
            requetesDisponibleModel.addElement("Il n'y a pas de requêtes pour le moment.");
            requetesDispo.setModel(requetesDisponibleModel);
            prendreRequeteButton.setEnabled(false);
        }
    }
}
