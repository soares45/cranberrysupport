package ca.cranberry.app.view;

import static ca.cranberry.app.utils.ISize.HORIZONTAL_CONNEXION_SIZE_FEN_CLIENT;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_FIELD_SIZE_FEN_CLIENT;
import static ca.cranberry.app.utils.ISize.HORIZONTAL_TOP_FIELD_GAP_FEN_CLIENT;
import static ca.cranberry.app.utils.ISize.VERTICAL_FIELD_GAP_FEN_CLIENT;
import static ca.cranberry.app.utils.ISize.VERTICAL_RESET_BUTTON_GAP_FEN_CLIENT;
import static ca.cranberry.app.utils.ISize.VERTICAL_TOP_GAP_FEN_CLIENT;
import static ca.cranberry.app.utils.IView.LABEL_ANNULER;
import static ca.cranberry.app.utils.IView.LABEL_CONNEXION;
import static ca.cranberry.app.utils.IView.LABEL_NAME;
import static ca.cranberry.app.utils.IView.LABEL_NAME_EXAMPLE;
import static ca.cranberry.app.utils.IView.LABEL_PASSWD;
import static ca.cranberry.app.utils.IView.LABEL_PASSWD_EXAMPLE;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

import ca.cranberry.app.beans.Utilisateur;
import ca.cranberry.app.exception.NomUtilisateurException;
import ca.cranberry.app.service.BanqueUtilisateurs;


public class FrenetreClient extends JFrame {

    private static final long serialVersionUID = 7501105166291662047L;
    private JButton resetButton;
    private JLabel motPasseExempleJLabel;
    private JLabel nameExempleJLabel;
    private JLabel motPasseJLabel;
    private JLabel nameJLabel;
    private JButton seConnecterButton;
    private JTextField motPasseJField;
    private JTextField utilisateurJField;
    private String nomUtil;
    private String motdepasse;
    private Utilisateur potentiel;

    public FrenetreClient() {
        initializeJLabel();
        initializeButtonConnecter();
        initializeButtonReset();
        utilisateurJField = new JTextField();
        motPasseJField = new JTextField();
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layoutSetHorizontal(layout);
        layoutSetVertical(layout);
        pack();
        setVisible(true);
    }

    private void layoutSetVertical(GroupLayout layout) {
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup().addGap(VERTICAL_TOP_GAP_FEN_CLIENT,VERTICAL_TOP_GAP_FEN_CLIENT,VERTICAL_TOP_GAP_FEN_CLIENT)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nameJLabel)
                                .addComponent(utilisateurJField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                        GroupLayout.PREFERRED_SIZE)
                                .addComponent(nameExempleJLabel))
                        .addGap(VERTICAL_FIELD_GAP_FEN_CLIENT,VERTICAL_FIELD_GAP_FEN_CLIENT,VERTICAL_FIELD_GAP_FEN_CLIENT)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(motPasseJLabel)
                                .addComponent(motPasseJField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                        GroupLayout.PREFERRED_SIZE)
                                .addComponent(motPasseExempleJLabel))
                        .addGap(VERTICAL_FIELD_GAP_FEN_CLIENT,VERTICAL_FIELD_GAP_FEN_CLIENT,VERTICAL_FIELD_GAP_FEN_CLIENT).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(resetButton).addComponent(seConnecterButton))
                        .addContainerGap(VERTICAL_RESET_BUTTON_GAP_FEN_CLIENT, Short.MAX_VALUE)));
    }

    private void layoutSetHorizontal(GroupLayout layout) {
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
                GroupLayout.Alignment.TRAILING,
                layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup().addContainerGap()
                                .addComponent(seConnecterButton, GroupLayout.PREFERRED_SIZE, HORIZONTAL_CONNEXION_SIZE_FEN_CLIENT,
                                        GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(resetButton))
                        .addGroup(
                                layout.createSequentialGroup().addGap(HORIZONTAL_TOP_FIELD_GAP_FEN_CLIENT,HORIZONTAL_TOP_FIELD_GAP_FEN_CLIENT,HORIZONTAL_TOP_FIELD_GAP_FEN_CLIENT)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                .addComponent(nameJLabel).addComponent(motPasseJLabel))
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                .addComponent(motPasseJField, GroupLayout.DEFAULT_SIZE, HORIZONTAL_FIELD_SIZE_FEN_CLIENT,
                                                        Short.MAX_VALUE)
                                                .addComponent(utilisateurJField, GroupLayout.DEFAULT_SIZE, HORIZONTAL_FIELD_SIZE_FEN_CLIENT,
                                                        Short.MAX_VALUE))))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(motPasseExempleJLabel).addComponent(nameExempleJLabel))
                        .addContainerGap()));
    }

    private void initializeJLabel() {
        nameJLabel = new JLabel();
        motPasseJLabel = new JLabel();
        nameExempleJLabel = new JLabel();
        motPasseExempleJLabel = new JLabel();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        nameJLabel.setText(LABEL_NAME);
        motPasseJLabel.setText(LABEL_PASSWD);
        nameExempleJLabel.setText(LABEL_NAME_EXAMPLE);
        motPasseExempleJLabel.setText(LABEL_PASSWD_EXAMPLE);
    }

    private void initializeButtonReset() {
        resetButton = new JButton();
        resetButton.setText(LABEL_ANNULER);
        resetButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                resetButtonMouseClicked(evt);
            }
        });
    }

    private void initializeButtonConnecter() {
        seConnecterButton = new JButton();
        seConnecterButton.setText(LABEL_CONNEXION);
        seConnecterButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                seConnecterButtonMouseClicked(evt);
            }
        });
    }

    private void seConnecterButtonMouseClicked(MouseEvent evt) {
        try {
            nomUtil = utilisateurJField.getText();
            motdepasse = motPasseJField.getText();

            try {
                potentiel = BanqueUtilisateurs.getInstance().getUtilisateurByNomUtilisateur(nomUtil);
                
                if (potentiel.login(nomUtil, motdepasse)) {
                    this.setVisible(false);
                    FenetreClientConnection clientLoggedFrame = new FenetreClientConnection(potentiel);
                } else {
                    FenetreErrorLogin errorLoginFrame = new FenetreErrorLogin();
                    this.setVisible(false);
                }
            } catch (NomUtilisateurException e) {
                FenetreErrorLogin errorLoginFrame = new FenetreErrorLogin();
                this.setVisible(false);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(FrenetreClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void resetButtonMouseClicked(MouseEvent evt) {
        this.setVisible(false);
        FenetreCranberry cranberryFrame = new FenetreCranberry();
    }

}
