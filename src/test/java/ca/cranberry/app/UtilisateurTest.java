package ca.cranberry.app;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ca.cranberry.app.beans.Requete;
import ca.cranberry.app.beans.Utilisateur;

class UtilisateurTest {
    private Utilisateur client;
    private Utilisateur technicien;
    private Requete requete;
    private String prenom = "prenom";
    private String nom = "nom";
    private String roleClient = "client";
    private String password = "password";
    private String roleTechnicien = "technicien";
    private String username = "username";
	private String motInutile = "naeodfjowejcfksjvglawrigwjmiofiowmioefowniofioj";
	private String sujet = "sujet";
	private String description = "description";
	private Requete.Categorie cat = Requete.Categorie.autre;

	@BeforeEach
	void setUp() throws Exception {
	    client = new Utilisateur(prenom, nom, username, password, Utilisateur.Role.fromString(roleClient));
		technicien = new Utilisateur(prenom, nom, username, password, Utilisateur.Role.fromString(roleTechnicien));
		requete = new Requete(sujet, description, client, cat);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void loginTrueTest() {
		assertTrue(technicien.login(username, password));
	}

	@Test
	void loginFalseTest() {
		assertFalse(technicien.login(username, motInutile));
	}

    @Test
    void ajouterRequeteAssigneeTest() {
        technicien.ajouterRequete(requete);
        assertTrue(technicien.getListStatut(Requete.Statut.ouvert).size() > 0);
    }

    @Test
    void ajoutListRequetesFiniesTest() {
        technicien.ajouterRequete(requete);
        requete.setStatut(Requete.Statut.finalSucces);
        technicien.ajouterRequete(requete);
        assertTrue(technicien.getListStatut(Requete.Statut.finalSucces).size() > 0
                && technicien.getListStatut(Requete.Statut.enTraitement).size() == 0);
    }

    @Test
    void ajoutRequeteTest() {
        technicien.ajouterRequete(requete);
        assertTrue(technicien.getRequetes().size() > 0);
    }

}
